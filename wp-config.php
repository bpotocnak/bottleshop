<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'c197_bottleshop');

/** MySQL database username */
define('DB_USER', 'c197_bottleshop');

/** MySQL database password */
define('DB_PASSWORD', 'B0ttl3sh0p@@10');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#b}gXd@M>j1U2;Ly5f/3-p+}GO5KZ@GJt63qHRWo/hv@4y2hm fT,,q*L0pm{ $$');
define('SECURE_AUTH_KEY',  '7?2>L<S1^|MFJQ^F[CEjFzWmzXd5VpNI?S4o#]^ez2E?3=?-(R73-=APX:9]qT95');
define('LOGGED_IN_KEY',    '-1VGcOlXmZ-|C_+e&lD6VZV-9[-qBlg0>+&))e91%|jB%9)yPDqJD|G]a `Mg^&+');
define('NONCE_KEY',        'yp/QnA|sD,,{^8eQTx6%1K,z;T9>5?|xi.B;kXqO[7>kL(&{hy$I(3I<Y+vQ},-U');
define('AUTH_SALT',        'ma`N@|D1<4o6;pI]&j-;[Lh;WUJJjWBMrI+hCFfE&!2C$,*k1B:uHgHTL!zY>Fe}');
define('SECURE_AUTH_SALT', '=:8:@+-?3ED<KJl7o<^7C<jem;k?uoPwZz<ggV7pl_3Jm2IE Kx:EG-Ciazz*}13');
define('LOGGED_IN_SALT',   'd*B:=%14}V2D5dqLGG=-VJ*6<p[<Idj>lR}:{+G{v0B4_j.>[p-FV%yYI_ZjS|c)');
define('NONCE_SALT',       'g07/?~2.FMBeH(9dAO$x^fx#MhF(Qaz,ZIDZvB+mUJSL@evdh,E6[D`mhys K!BV');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'shop_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
